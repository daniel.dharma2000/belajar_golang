package main

import (
	_ "hello/routers"
	"github.com/astaxie/beego"
	"github.com/beego/beego/v2/client/orm"

	//_ "github.com/go-sql-driver/mysql" 
	_ "github.com/lib/pq"
)

func init() {
	
	err := orm.RegisterDriver("postgres", orm.DRPostgres)
	if(err != nil) {
		beego.Critical(err)
	}
	
    err2 := orm.RegisterDataBase(
		"default", 
        "postgres",
		"host=127.0.0.1 user=daniel password=mypw dbname=ottoJunior sslmode=disable");
	
	if(err2 != nil) {
		beego.Critical(err2)
	}
	
	/*
	orm.RegisterDriver("mysql", orm.DRMySQL)
	err := orm.RegisterDataBase("default", "mysql", "daniel:January111988@tcp(rm-d9jkmzu5dlupq5392so.mysql.ap-southeast-5.rds.aliyuncs.com:3306)/lakuemas_preprod_empty")
	if(err != nil) {
		beego.Critical(err)
	}
	*/
	orm.RunSyncdb("default", false, false)
	orm.RunCommand()
}

func main() {
	beego.Run()
}

