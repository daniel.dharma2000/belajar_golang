package routers

import (
	"hello/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
	beego.Router("/price", &controllers.PriceController{})
	//beego.Router("/coba", &controllers.CobaController{})
}
