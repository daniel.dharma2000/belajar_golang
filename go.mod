module hello

go 1.18

require github.com/astaxie/beego v1.12.1

require (
	github.com/beego/beego/v2 v2.0.5
	github.com/go-sql-driver/mysql v1.6.0
	github.com/lib/pq v1.10.5
	github.com/smartystreets/goconvey v1.6.4
)

require (
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
