package models

import (
	"time"
    "github.com/beego/beego/v2/client/orm"
)

type (
	User struct {
		Id   		int 		`orm:"auto"`
		Name 		string 		`orm:"column(name)"`
		Email 		string 		`orm:"column(email)"`
		Handphone	string 		`orm:"column(handphone)"`
		Password 	string 		`orm:"column(password)" json:"-"`
		Created_at 	time.Time 	`orm:"auto_now_add;type(datetime);column(created_at)"`
		Updated_at 	time.Time 	`orm:"auto_now;type(datetime);column(updated_at)"`
	}
)

func init() {
	orm.RegisterModel(new(User))
}

func GetAllUsers() ([]User) {
	o := orm.NewOrm()
	var users []User
	o.QueryTable(new(User)).All(&users)
	return users
}

func GetUserDetail(id string) (User) {
	o := orm.NewOrm()
	var user User
	o.QueryTable(new(User)).Filter("Id", id).One(&user);
	return user
}

func Insert(
	name string, 
	email string, 
	handphone string, 
	password string) () {
	o := orm.NewOrm()
	user := User{
		Name		: name,
		Email		: email,
		Handphone	: handphone,
		Password 	: password,
	}
	o.Insert(&user)
}

func Update(
	id int,
	name string, 
	email string, 
	handphone string, 
	password string) {

	o := orm.NewOrm()
	var user User
	o.QueryTable(new(User)).Filter("Id", id).One(&user);
	
	user.Name = name
	user.Email = email
	user.Handphone = handphone
	user.Password = password
	o.Update(&user)
}